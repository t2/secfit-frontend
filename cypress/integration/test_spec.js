const userId1 = 1
const username1 = "endre"
const birthday1 = "2021-03-12"
const bio1 = "BIO"
const favoriteExercise1 = "Benkpress"

const userId2 = 2
const username2 = "sig"
const birthday2 = "2021-03-12"
const bio2 = "BIO"
const favoriteExercise2 = "Benkpress"

const userId3 = 3
const username3 = "sigmund"
const birthday3 = "2021-03-12"
const bio3 = "BIO"
const favoriteExercise3 = "Benkpress"

const userResults1 = {
  "url": `https://secfit-t2-backend.herokuapp.com/api/users/${userId1}/`,
  "id": userId1,
  "email": "",
  "username": username1,
  "athletes": [`https://secfit-t2-backend.herokuapp.com/api/users/${userId2}/`],
  "phone_number": "",
  "country": "",
  "city": "",
  "street_address": "",
  "coach": null,
  "workouts": [],
  "coach_files": [],
  "athlete_files": ["https://secfit-t2-backend.herokuapp.com/api/athlete-files/1/"],
  "bio": bio1,
  "birthday": birthday1,
  "favorite_exercise": favoriteExercise1
}

const userResults2 = {
  "url": `https://secfit-t2-backend.herokuapp.com/api/users/${userId2}/`,
  "id": userId2,
  "email": "",
  "username": username2,
  "athletes": [],
  "phone_number": "",
  "country": "",
  "city": "",
  "street_address": "",
  "coach": `https://secfit-t2-backend.herokuapp.com/api/users/${userId1}/`,
  "workouts": [],
  "coach_files": [],
  "athlete_files": [],
  "bio": bio2,
  "birthday": birthday2,
  "favorite_exercise": favoriteExercise2
}

const userResults3 = {
  "url": `https://secfit-t2-backend.herokuapp.com/api/users/${userId3}/`,
  "id": userId3,
  "email": "",
  "username": username3,
  "athletes": [],
  "phone_number": "",
  "country": "",
  "city": "",
  "street_address": "",
  "coach": null,
  "workouts": [],
  "coach_files": [],
  "athlete_files": [],
  "bio": bio3,
  "birthday": birthday3,
  "favorite_exercise": favoriteExercise3
}

const responseUser1 = {
  "count": 1,
  "next": null,
  "previous": null,
  "results": [
    userResults1
  ]
}

const exerciseResults = { 
  "count": 2, 
  "next": null, 
  "previous": null, 
  "results": [
    { 
      "url": "https://secfit-t2-backend.herokuapp.com/api/exercises/1/", 
      "id": 1, 
      "name": "Benkpress", 
      "description": "Press vektstang", 
      "unit": "kg", 
      "instances": [] 
    }, 
    {
      "url": "https://secfit-t2-backend.herokuapp.com/api/exercises/2/", 
      "id": 2, 
      "name": "Knebøy", 
      "description": "Løft vektstang på ryggen", 
      "unit": "kg", 
      "instances": [] 
    }
  ]
}

const newUsername = "endre_edit"
const newBirthday = "1997-11-10"
const newBio = "NY BIO"
const newFavoriteExercise = "Knebøy"

const updatedUser = { 
  "url": `https://secfit-t2-backend.herokuapp.com/api/users/${userId1}/`, 
  "id": userId1, 
  "email": "", 
  "username": newUsername, 
  "athletes": [], 
  "phone_number": "", 
  "country": "", 
  "city": "", 
  "street_address": "", 
  "coach": null, 
  "workouts": [], 
  "coach_files": [], 
  "athlete_files": [], 
  "bio": newBio, 
  "birthday": newBirthday, 
  "favorite_exercise": newFavoriteExercise 
}

const usersResult = {
  "count": 2,
  "next": null,
  "previous": null,
  "results": [
  userResults1, 
  userResults2, 
  userResults3
]}

const workoutId1 = 1
const workoutId2 = 2

const workout1 = {
  "url": `https: //secfit-t2-backend.herokuapp.com/api/workouts/${workoutId1}/`,
  "id": workoutId1,
  "name": "Trening",
  "date": "2021-03-11T08:03:00Z",
  "notes": "Notat",
  "owner": `https: //secfit-t2-backend.herokuapp.com/api/users/${userId1}/`,
  "owner_username": username1,
  "visibility": "PU",
  "exercise_instances": [
    // {
    //   "url": "https://secfit-t2-backend.herokuapp.com/api/exercise-instances/1/",
    //   "id": 1,
    //   "exercise": "https://secfit-t2-backend.herokuapp.com/api/exercises/2/",
    //   "sets": 1,
    //   "number": 10,
    //   "workout": "https://secfit-t2-backend.herokuapp.com/api/workouts/1/"
    // }
  ],
  "files": []
}

const workout2 = {
  "url": `https: //secfit-t2-backend.herokuapp.com/api/workouts/${workoutId2}/`,
  "id": workoutId2,
  "name": "Trening",
  "date": "2021-03-10T08:04:00Z",
  "notes": "Notat",
  "owner": `https: //secfit-t2-backend.herokuapp.com/api/users/${userId2}/`,
  "owner_username": username2,
  "visibility": "PU",
  "exercise_instances": [
    // {
    //   "url": "https://secfit-t2-backend.herokuapp.com/api/exercise-instances/2/",
    //   "id": 2,
    //   "exercise": "https://secfit-t2-backend.herokuapp.com/api/exercises/2/",
    //   "sets": 10,
    //   "number": 10,
    //   "workout": "https://secfit-t2-backend.herokuapp.com/api/workouts/2/"
    // }
  ],
  "files": []
}

const workoutsResult = {
  "count": 2,
  "next": null,
  "previous": null,
  "results": [workout1, workout2]
}

const commentsResult = {
  "count": 0,
  "next": null,
  "previous": null,
  "results": []
}

const offersResult = {
  "count": 0, 
  "next": null, 
  "previous": null, 
  "results": []
}

const exercise = {
  "url": "https://secfit-t2-backend.herokuapp.com/api/exercises/1/", 
  "id": 1, 
  "name": "Benkpress", 
  "description": "Press vektstang", 
  "unit": "kg", 
  "instances": []
}

const exercise2 = {
  "url": "https://secfit-t2-backend.herokuapp.com/api/exercises/1/", 
  "id": 1, 
  "name": "Knebøy", 
  "description": "Press vektstang", 
  "unit": "kg", 
  "instances": []
}

const fileName = "filnavn.txt"

const athleteFilesResult = {
  "url":"https://secfit-t2-backend.herokuapp.com/api/athlete-files/1/",
  "id":1,
  "owner":"coach",
  "file":`https://secfit-t2-backend.herokuapp.com/media/users/${userId2}/${fileName}`,
  "athlete":`https://secfit-t2-backend.herokuapp.com/api/users/${userId2}/`
}

// const wor

// describe('profilepage', () => {
//   beforeEach(() => {
//     cy.visit('/')
//     cy.contains('Log in').click()
//     console.log(cy.get('input'))
//     cy.get('input[name="username"]').type('endre')
//     cy.get('input[name="password"]').type('endre')
//     cy.intercept(
//       {
//         method: 'POST',      // Route all GET requests
//         url: 'https://secfit-t2-backend.herokuapp.com/api/token/',    // that have a URL that matches '/users/*'
//       },
//       { 
//         "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYxNTE5NTMzMiwianRpIjoiZTEyYzU5YjcwNzBjNGJlMjhiNzRkNGJlY2E3NmVhYjciLCJ1c2VyX2lkIjo0fQ.qCmqEjEIXsic1YEE1teZmNJc5ggdaFBEm_ab0rUvdoQ", 
//         "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE1MTA5MjMyLCJqdGkiOiIwZGE4MWQzZjIzOTQ0YTI1OGFjYTI2OWViMzkwNWViMyIsInVzZXJfaWQiOjR9.eKNn54JEkHaP7ny4VZ2Pazwa9diphAe1BPCXTaG7BMs" 
//       }// and force the response to be: []
//     )
//     cy.intercept(
//       {
//         method: 'POST',      // Route all GET requests
//         url: 'https://secfit-t2-backend.herokuapp.com/api/token/*',    // that have a URL that matches '/users/*'
//       },
//       { 
//         "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYxNTE5NTMzMiwianRpIjoiZTEyYzU5YjcwNzBjNGJlMjhiNzRkNGJlY2E3NmVhYjciLCJ1c2VyX2lkIjo0fQ.qCmqEjEIXsic1YEE1teZmNJc5ggdaFBEm_ab0rUvdoQ", 
//         "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE1MTA5MjMyLCJqdGkiOiIwZGE4MWQzZjIzOTQ0YTI1OGFjYTI2OWViMzkwNWViMyIsInVzZXJfaWQiOjR9.eKNn54JEkHaP7ny4VZ2Pazwa9diphAe1BPCXTaG7BMs" 
//       }// and force the response to be: []
//     )
//     cy.intercept(
//       {
//         method: 'GET',      // Route all GET requests
//         url: 'https://secfit-t2-backend.herokuapp.com/api/users/?user=current',    // that have a URL that matches '/users/*'
//       },
//       responseUser1
//     )
//     cy.intercept(
//       {
//         method: 'GET',      // Route all GET requests
//         url: `https://secfit-t2-backend.herokuapp.com/api/users/${userId1}`, 
//       },
//       userResults1
//     )
//     cy.intercept(
//       {
//         method: 'GET',      // Route all GET requests
//         url: `https://secfit-t2-backend.herokuapp.com/api/users/${userId2}`, 
//       },
//       userResults2
//     ).as('getUser2')
//     cy.intercept(
//       {
//         method: 'GET',      // Route all GET requests
//         url: `https://secfit-t2-backend.herokuapp.com/api/users/`, 
//       },
//       usersResult
//     )
//     cy.intercept(
//       {
//         method: 'GET',      // Route all GET requests
//         url: `https://secfit-t2-backend.herokuapp.com/api/exercises/`, 
//       },
//       exerciseResults
//     )
//     cy.intercept(
//       {
//         method: 'PATCH',      
//         url: `https://secfit-t2-backend.herokuapp.com/api/users/${userId1}`, 
//       },
//       updatedUser
//     ).as('updateUser')
//     cy.intercept(
//       {
//         method: 'GET',      
//         url: `https://secfit-t2-backend.herokuapp.com/api/workouts/?ordering=-date`, 
//       },
//       workoutsResult
//     )
//     cy.intercept(
//       {
//         method: 'GET',      
//         url: `https://secfit-t2-backend.herokuapp.com/api/workouts/${workoutId2}/`, 
//       },
//       workout2
//     ).as('populateWorkout')
//     cy.intercept(
//       {
//         method: 'GET',      
//         url: `https://secfit-t2-backend.herokuapp.com/api/comments/`, 
//       },
//       commentsResult
//     )
//     cy.intercept(
//       {
//         method: 'GET',      
//         url: `https://secfit-t2-backend.herokuapp.com/api/offers/*`, 
//       },
//       offersResult
//     )
//     cy.intercept(
//       {
//         method: 'GET',      
//         url: `https://secfit-t2-backend.herokuapp.com/api/athlete-files/*`, 
//       },
//       athleteFilesResult
//     )
//     cy.get('#btn-login').click()
//     cy.visit('/')
//   });
//   it('render own profilepage', () => {
//     cy.visit('/profilepage.html')
//     cy.get("#username").should("have.value", username1)
//     cy.get("#birthday").should("have.value", birthday1)
//     cy.get("#favoriteExercise").should("have.value", favoriteExercise1)
//     cy.get("#bio").should("have.value", bio1)
//   })

//   it('render other profilepage', () => {
//     cy.visit(`/profilepage.html?id=${userId2}`)
//     cy.get("#username").should("have.value", username2)
//     cy.get("#birthday").should("have.value", birthday2)
//     cy.get("#favoriteExercise").should("have.value", favoriteExercise2)
//     cy.get("#bio").should("have.value", bio2)
//   })

//   it('edit profilepage', () => {
//     cy.visit(`/profilepage.html`)
//     cy.get('#edit-profile-page-button').click()
//     cy.get("#username").clear().type(newUsername).should("have.value", newUsername)
//     cy.get("#birthday").clear().type(newBirthday).should("have.value", newBirthday)
//     cy.get("#favoriteExercise-select").select(newFavoriteExercise).should("have.value", newFavoriteExercise)
//     cy.get("#bio").clear().type(newBio).should("have.value", newBio)
//     cy.get("#save-profile-page-button").click()
//     cy.wait('@updateUser').then(interception => {
//       expect(interception.response.body.username).to.equal(newUsername)
//       expect(interception.response.body.birthday).to.equal(newBirthday)
//       expect(interception.response.body.bio).to.equal(newBio)
//       expect(interception.response.body.favorite_exercise).to.equal(newFavoriteExercise)
//     })
//   })
  
//   it('search profile', () => {
//     cy.visit('/')
//     cy.get('#profileSearch').type(username2)
//     cy.get('#profiles option').should('have.length', 2)
//       .first().should('have.value', username2)
//       .next().should('have.value', username3)
//     cy.get('.search-submit').click()
//     cy.get("#username").should("have.value", username2)
//     cy.get("#birthday").should("have.value", birthday2)
//     cy.get("#favoriteExercise").should("have.value", favoriteExercise2)
//     cy.get("#bio").should("have.value", bio2)
//   })
  
//   it('go to profile from workouts', () => {
//     cy.visit('/workouts.html')
//     cy.get('tr > td.pointer').contains(username2).click()
//     cy.get("#username").should("have.value", username2)
//     cy.get("#birthday").should("have.value", birthday2)
//     cy.get("#favoriteExercise").should("have.value", favoriteExercise2)
//     cy.get("#bio").should("have.value", bio2)
//   })

//   it('go to profile from workout', () => {
//     cy.visit(`/workout.html?id=${workoutId2}`)
//     cy.wait('@populateWorkout')
//     cy.get('#inputOwner').should("have.value", username2)
//     cy.get('#inputOwner').click()
//     cy.get("#username").should("have.value", username2)
//     cy.get("#birthday").should("have.value", birthday2)
//     cy.get("#favoriteExercise").should("have.value", favoriteExercise2)
//     cy.get("#bio").should("have.value", bio2)
//   })
  
//   it('go to profile with button', () => {
//     cy.get('#btn-username').click()
//     cy.get("#username").should("have.value", username1)
//     cy.get("#birthday").should("have.value", birthday1)
//     cy.get("#favoriteExercise").should("have.value", favoriteExercise1)
//     cy.get("#bio").should("have.value", bio1)
//   })

//   it('visit my athletes', () => {
//     cy.visit('/myathletes.html')
//     cy.get(`#tab-${username2}`).click()
//     cy.get(`#tab-contents-${username2} > div > a`).first().should("have.text", fileName)
//   })
  
// })

describe('exercise', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.contains('Log in').click()
    console.log(cy.get('input'))
    cy.get('input[name="username"]').type('endre')
    cy.get('input[name="password"]').type('endre')
    cy.intercept(
      {
        method: 'POST',      // Route all GET requests
        url: 'https://secfit-t2-backend.herokuapp.com/api/token/',    // that have a URL that matches '/users/*'
      },
      { 
        "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYxNTE5NTMzMiwianRpIjoiZTEyYzU5YjcwNzBjNGJlMjhiNzRkNGJlY2E3NmVhYjciLCJ1c2VyX2lkIjo0fQ.qCmqEjEIXsic1YEE1teZmNJc5ggdaFBEm_ab0rUvdoQ", 
        "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE1MTA5MjMyLCJqdGkiOiIwZGE4MWQzZjIzOTQ0YTI1OGFjYTI2OWViMzkwNWViMyIsInVzZXJfaWQiOjR9.eKNn54JEkHaP7ny4VZ2Pazwa9diphAe1BPCXTaG7BMs" 
      }// and force the response to be: []
    )
    cy.intercept(
      {
        method: 'POST',      // Route all GET requests
        url: 'https://secfit-t2-backend.herokuapp.com/api/token/*',    // that have a URL that matches '/users/*'
      },
      { 
        "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYxNTE5NTMzMiwianRpIjoiZTEyYzU5YjcwNzBjNGJlMjhiNzRkNGJlY2E3NmVhYjciLCJ1c2VyX2lkIjo0fQ.qCmqEjEIXsic1YEE1teZmNJc5ggdaFBEm_ab0rUvdoQ", 
        "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE1MTA5MjMyLCJqdGkiOiIwZGE4MWQzZjIzOTQ0YTI1OGFjYTI2OWViMzkwNWViMyIsInVzZXJfaWQiOjR9.eKNn54JEkHaP7ny4VZ2Pazwa9diphAe1BPCXTaG7BMs" 
      }// and force the response to be: []
    )
    cy.intercept(
      {
        method: 'GET',      // Route all GET requests
        url: 'https://secfit-t2-backend.herokuapp.com/api/users/?user=current',    // that have a URL that matches '/users/*'
      },
      responseUser1
    )
    cy.intercept(
      {
        method: 'GET',      // Route all GET requests
        url: 'https://secfit-t2-backend.herokuapp.com/api/exercises/1/',    // that have a URL that matches '/users/*'
      },
      exercise
    )
    cy.intercept(
      {
        method: 'PUT',      // Route all GET requests
        url: 'https://secfit-t2-backend.herokuapp.com/api/exercises/1/',    // that have a URL that matches '/users/*'
      },
      exercise
    )
    cy.intercept(
      {
        method: 'GET',      // Route all GET requests
        url: `https://secfit-t2-backend.herokuapp.com/api/users/${userId1}`, 
      },
      userResults1
    )
    cy.intercept(
      {
        method: 'GET',      // Route all GET requests
        url: `https://secfit-t2-backend.herokuapp.com/api/users/${userId2}`, 
      },
      userResults2
    ).as('getUser2')
    cy.intercept(
      {
        method: 'GET',      // Route all GET requests
        url: `https://secfit-t2-backend.herokuapp.com/api/users/`, 
      },
      usersResult
    )
    cy.intercept(
      {
        method: 'GET',      // Route all GET requests
        url: `https://secfit-t2-backend.herokuapp.com/api/exercises/`, 
      },
      exerciseResults
    )
    cy.intercept(
      {
        method: 'PATCH',      
        url: `https://secfit-t2-backend.herokuapp.com/api/users/${userId1}`, 
      },
      updatedUser
    ).as('updateUser')
    cy.intercept(
      {
        method: 'GET',      
        url: `https://secfit-t2-backend.herokuapp.com/api/workouts/?ordering=-date`, 
      },
      workoutsResult
    )
    cy.intercept(
      {
        method: 'GET',      
        url: `https://secfit-t2-backend.herokuapp.com/api/workouts/${workoutId2}/`, 
      },
      workout2
    ).as('populateWorkout')
    cy.intercept(
      {
        method: 'GET',      
        url: `https://secfit-t2-backend.herokuapp.com/api/comments/`, 
      },
      commentsResult
    )
    cy.intercept(
      {
        method: 'GET',      
        url: `https://secfit-t2-backend.herokuapp.com/api/offers/*`, 
      },
      offersResult
    )
    cy.intercept(
      {
        method: 'GET',      
        url: `https://secfit-t2-backend.herokuapp.com/api/athlete-files/*`, 
      },
      athleteFilesResult
    )
    cy.get('#btn-login').click()
    cy.visit('/')
  });


  it('deleteOldFormData exercises', () => {
    cy.visit('/exercise.html?id=1')
    cy.get('#btn-edit-exercise').click()
    cy.get('#btn-edit-exercise').click()
    cy.get('#inputName').type('Knebøy')
    cy.get('#btn-cancel-exercise').click()
    cy.get('#inputName').should("have.value", "Benkpress")

    cy.get('#btn-edit-exercise').click()
    cy.get('#inputName').clear()
    cy.get('#inputName').type('Knebøy')
    cy.get('#btn-ok-exercise').click()
    cy.get('#inputName').should("have.value", "Knebøy")
  })

})