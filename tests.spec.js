const fs = require('fs');
const path = require('path');
const html = fs.readFileSync(path.resolve(__dirname, 'www/profilepage.html'), 'utf8');
let { getUserById, onDOMContentLoaded, handleEditProfileButtonClick } = require('./www/scripts/profilepage.js');
let { getCurrentUser, getExerciseTypesResponse, fetchProfiles } = require('./www/scripts/scripts.js');
const fetch = require('node-fetch');
const { Response } = jest.requireActual('node-fetch')
const { searchFunctionality } = require('./www/scripts/search')
const NavBar = require('./www/scripts/navbar')

// jest
//     .dontMock('fs');

jest
    .mock('./www/scripts/scripts.js')
    //.mock('./www/scripts/profilepage.js')

describe('profilepage',  function () {
    beforeEach(() => {
        document.body.innerHTML = html.toString();
        window.script = {oboServicePath: './www/scripts/scripts.js'};
        window.script = {oboServicePath: './www/scripts/profilepage.js'};
    });

    afterEach(() => {
        // restore the original func after test
        jest.resetModules();
    });

    it('render own profilepage', async function () {
        const user = {username: "havard", birthday:"1212-12-20", favorite_exercise:"benk", bio:"strong"}
        getCurrentUser.mockResolvedValue(user)
        await onDOMContentLoaded()
        expect(document.getElementById('bio').value).toBe(user.bio);
        expect(document.getElementById('favoriteExercise').value).toBe(user.favorite_exercise);
        expect(document.getElementById('birthday').value).toBe(user.birthday);
        expect(document.getElementById('username').value).toBe(user.username);
    });

    it('render other profilepage', async function () {
        window.history.pushState({}, 'Test Title', 'asd.no?id=1');
        const user = {username: "havard", birthday:"1212-12-20", favorite_exercise:"benk", bio:"strong"}
        getUserById.mockResolvedValue(user)
        await onDOMContentLoaded();
        expect(document.getElementById('bio').value).toBe(user.bio);
        expect(document.getElementById('favoriteExercise').value).toBe(user.favorite_exercise);
        expect(document.getElementById('birthday').value).toBe(user.birthday);
        expect(document.getElementById('username').value).toBe(user.username);
    });

    it('edit profilepage', async function () {
        window.history.pushState({}, 'Test Title', 'asd.no?id=1');
        const user = {username: "havard", birthday:"1212-12-20", favorite_exercise:"benk", bio:"strong"}
        getUserById.mockResolvedValue(user);
        const exercises_response = new Response({ 
            "count": 3, 
            "next": null, 
            "previous": null, 
            "results": [
                { 
                    "url": "http://molde.idi.ntnu.no:21005/api/exercises/1/", 
                    "id": 1, 
                    "name": "Push-up", 
                    "description": "A push-up (or press-up in British English) is a common calisthenics exercise beginning from the prone position.", 
                    "unit": "reps", 
                    "instances": [
                        "http://molde.idi.ntnu.no:21005/api/exercise-instances/1/", 
                        "http://molde.idi.ntnu.no:21005/api/exercise-instances/2/"
                    ] 
                }, 
                { 
                    "url": "http://molde.idi.ntnu.no:21005/api/exercises/2/", 
                    "id": 2, 
                    "name": "Crunch", 
                    "description": "The crunch is one of the most popular abdominal exercises.", 
                    "unit": "reps", 
                    "instances": [] 
                }, 
                { 
                    "url": "http://molde.idi.ntnu.no:21005/api/exercises/3/", 
                    "id": 3, 
                    "name": "Plank", 
                    "description": "The plank is an isometric core strength exercise that involves maintaining a position similar to a push-up for the maximum possible time.", 
                    "unit": "seconds", 
                    "instances": [] 
                }
            ] 
        },
        {
            "status": 200,
            "statusText": "OK"
        })
        getExerciseTypesResponse.mockResolvedValue(exercises_response);
        await onDOMContentLoaded();
        expect(document.getElementById('bio').readOnly).toBe(true);
        await handleEditProfileButtonClick();
        expect(document.getElementById('bio').readOnly).toBe(false);
    });

    it('test search', async function() {
        const users_response = [{ "url": "https://secfit-t2-backend.herokuapp.com/api/users/1/", "id": 1, "email": "", "username": "sig", "athletes": [], "phone_number": "", "country": "", "city": "", "street_address": "", "coach": null, "workouts": [], "coach_files": [], "athlete_files": [], "bio": "", "birthday": null, "favorite_exercise": "" }, { "url": "https://secfit-t2-backend.herokuapp.com/api/users/2/", "id": 2, "email": "", "username": "Sigmund", "athletes": [], "phone_number": "", "country": "", "city": "", "street_address": "", "coach": null, "workouts": [], "coach_files": [], "athlete_files": [], "bio": "Jeg er god", "birthday": "2021-03-10", "favorite_exercise": "" }, { "url": "https://secfit-t2-backend.herokuapp.com/api/users/3/", "id": 3, "email": "", "username": "Endre", "athletes": [], "phone_number": "", "country": "", "city": "", "street_address": "", "coach": null, "workouts": [], "coach_files": [], "athlete_files": [], "bio": "Liker å dra på portalen", "birthday": "2017-01-17", "favorite_exercise": "" }]
        const expected_search_list = '<option value="sig"></option><option value="Sigmund"></option><option value="Endre"></option>'
        fetchProfiles.mockResolvedValue(users_response)
        const navbar = new NavBar();
        navbar.connectedCallback()
        document.body.innerHTML = navbar.innerHTML
        await searchFunctionality()
        const profileSearch = document.getElementById('profileSearch');
        const event = new Event('input')
        profileSearch.dispatchEvent(event)
        function sleep(ms) {
            return new Promise((resolve) => {
                setTimeout(resolve, ms);
            });
        }
        await sleep(500)
        expect(document.getElementById('profiles').innerHTML).toBe(expected_search_list)
    })
});