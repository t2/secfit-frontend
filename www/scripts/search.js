//Contains the main parts of the search functionality, namely reading the input and displaying options based on calls to the backend.
async function searchFunctionality() {
    const form = document.getElementById("searchForm");
    function handleForm(event) { event.preventDefault(); };
    form.addEventListener('submit', handleForm);
    const profileSearch = document.getElementById('profileSearch');

    const users = await fetchProfiles()

    profileSearch.addEventListener("input", async e => {
        const searchString = e.target.value;
        let profileMatches = users.filter((user) => user.username.toLowerCase().includes(searchString.toLowerCase()))
        var options = '';
        for (var i = 0; i < profileMatches.length; i++) {
            options += '<option value="' + profileMatches[i].username + '" />';
        }
        document.getElementById('profiles').innerHTML = options;
    })
};

//This function fetches every profile from the backend 
async function fetchProfiles() {
    let users;
    let response = await sendRequest("GET", `${HOST}/api/users/`);
    if (response.ok) {
        let data = await response.json();
        users = data.results;
    }
    return users

};
//handles the search button and redirects the user to profile page
async function profileSearchSubmitted(event) {
    const users = await fetchProfiles()

    const searchValue = document.getElementById("profileSearch").value

    let profileMatches = users.filter((user) => user.username === searchValue);

    //if there were no exact match, then check for partial match
    if (profileMatches.length === 0){
        profileMatches = users.filter((user) => user.username.includes(searchValue));
    }

    try {
        window.location.assign(`profilepage.html?id=${profileMatches[0].id}`);
    } catch (error) {
        window.alert("No partial or exact mathing profile found");
    };
}

//run the main search function after the window have finished loading
window.addEventListener('DOMContentLoaded', searchFunctionality);