async function getUserById(id) {
    let user;
    let response = await sendRequest("GET", `${HOST}/api/users/${id}`);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("404 Could not get user!", data);
        document.body.prepend(alert);
    } else {
        user = await response.json();
    }

    return user;
}

function displayProfilePage(user) {

    if(typeof user === 'undefined'){
        const editButton = document.getElementById('edit-profile-page-button')
        editButton.classList.add("hide")
    }
    else{
        const username = document.getElementById('username');
        username.value = user.username;
    
        const birthday = document.getElementById('birthday');
        birthday.value = user.birthday
        
        const favoriteExercise = document.getElementById('favoriteExercise');
        favoriteExercise.value = user.favorite_exercise

        const bio = document.getElementById('bio');
        bio.value = user.bio
    } 
}

function handleCancelDuringProfileEdit() {
    location.reload();
}

function createOption(favoriteExercise_select, value, name, favoriteExercise) {
    let option = document.createElement("option");
    option.value = value;
    option.innerText = name;
    favoriteExercise_select.append(option);
    if (favoriteExercise === value) option.selected = true;
}

async function handleEditProfileButtonClick(user) {
    const editProfileButton = document.querySelector("#edit-profile-page-button");
    const saveProfileButton = document.querySelector("#save-profile-page-button");
    const cancelProfileButton = document.querySelector("#cancel-profile-page-button");
    const favoriteExercise_select = document.getElementById('favoriteExercise-select');
    const favoriteExercise_select_container = document.getElementById('favoriteExercise-select-container');
    const favoriteExercise_container = document.getElementById('favoriteExercise-container');
    let exerciseTypeResponse = await sendRequest("GET", `${HOST}/api/exercises/`);
    let exerciseTypes = await exerciseTypeResponse.json();

    cancelProfileButton.addEventListener("click", handleCancelDuringProfileEdit);
    saveProfileButton.addEventListener("click", updateProfilePage);

    editProfileButton.classList.add("hide");
    favoriteExercise_container.classList.add("hide")
    saveProfileButton.classList.remove("hide");
    cancelProfileButton.classList.remove("hide");
    favoriteExercise_select_container.classList.remove("hide")

    favoriteExercise_select.disabled = false;
    for (let i = 0; i < exerciseTypes.count; i++) {
        createOption(favoriteExercise_select, exerciseTypes.results[i].name, exerciseTypes.results[i].name, user.favorite_exercise)
    }

    createOption(favoriteExercise_select, "", "Ingen favoritt", user.favorite_exercise)
    
    setReadOnly(false, "#form-profile");
}

async function updateProfilePage() {
    const user = await getCurrentUser();
    const username = document.getElementById('username').value;
    const birthday = document.getElementById('birthday').value;
    const favoriteExerciseDropdown = document.getElementById('favoriteExercise-select');
    const favoriteExercise = favoriteExerciseDropdown.options[favoriteExerciseDropdown.selectedIndex].value;
    const bio = document.getElementById('bio').value;
    user.username = username;
    user.birthday = birthday;
    user.favoriteExercise = favoriteExercise
    user.bio = bio

    let body = {
        'username': username,
        'birthday': birthday !== "" ? birthday : null,
        'favorite_exercise': favoriteExercise !== "" ? favoriteExercise : "",
        'bio': bio
    }

    let response = await sendRequest("PATCH", user.url, body);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not update profile!", data);
        document.body.prepend(alert);
    } else {
        location.reload();
    }
}


window.addEventListener("DOMContentLoaded", async () => {
    let user;
    const urlParams = new URLSearchParams(window.location.search);
    let editProfileButton = document.querySelector("#edit-profile-page-button");
    let currentUser = await getCurrentUser();


    if(urlParams.has('id')){ 
        let userId = urlParams.get('id');
        user = await getUserById(userId)
    }
    else{
        user = currentUser;
    }

    if (currentUser && user.username == currentUser.username) {
        editProfileButton.classList.remove("hide");
        editProfileButton.addEventListener("click", () => handleEditProfileButtonClick(user));
    }

    
    displayProfilePage(user)
});